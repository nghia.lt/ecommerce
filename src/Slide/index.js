import React from 'react';

class Slide extends React.Component{
    render(){
        return (
            <section className="slider-area-main">
                <div className="slider-area">
                    <div className="bend niceties preview-1">
                        <div id="ensign-nivoslider-3" className="slides">
                            <img src="/uploads/slide1.jpeg" alt="" title="#slider-direction-1" />
                        </div>
                    </div> 
                </div>
            </section>
        )
    }
}

export default Slide;