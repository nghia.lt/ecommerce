import React from 'react';

class Product extends React.Component{
    render(){
        return (
            <section className="featured-area">
                <div className="container">
                    <div className="row">
                        <div className="text-center">
                            <div className="section-titel">
                                <h3>Featured Products</h3>
                            </div>
                        </div>
                        <div className="featured-tab">
                            <ul className="text-center">
                                <li className="active"><a data-toggle="tab" href="#home">BOOK</a></li>
                                <li><a data-toggle="tab" href="#menu1">ELECTRONIC</a></li>
                                <li><a data-toggle="tab" href="#menu2">CLOTHING</a></li>
                            </ul>
                        </div>
                        <div className="tab-content">
                            <div id="home" className="tab-pane active">
                                <div id="features-curosel" className="indicator-style">
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/uploads/product-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale">
                                                <span className="sale-text">Sale</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">ÁO PHÔNG </a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">100$</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu1" className="tab-pane">
                                <div id="features-curosel-two" className="indicator-style">
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-4.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale">
                                                <span className="sale-text">Sale</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">35%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-4.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale">
                                                <span className="sale-text">Sale</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">45%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-4.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">45%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">45%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu2" className="tab-pane">
                                <div id="features-curosel-three" className="indicator-style">
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-4.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale">
                                                <span className="sale-text">Sale</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">30%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-3.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale">
                                                <span className="sale-text">Sale</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">95%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">95%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="single-product">
                                            <div className="product-image">
                                                <a className="product-img" href="/">
                                                    <img className="primary-img" src="/web/img/home-1/fp-2.jpg" alt="" />
                                                    <img className="secondary-img" src="/web/img/home-1/fp-1.jpg" alt="" />
                                                </a>
                                            </div>
                                            <span className="onsale red">
                                                <span className="sale-text">95%</span>
                                            </span>
                                            <div className="product-action">
                                                <h4><a href="/">Sanita Shoes</a></h4>
                                                <ul className="pro-rating">
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li className="pro-ratcolor"><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                    <li><i className="fa fa-star"></i></li>
                                                </ul>
                                                <span className="price">$ 180</span>
                                            </div>
                                            <div className="pro-action">
                                                <ul>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Shop Cart">
                                                            <i className="fa fa-retweet" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="test all_src_icon" href="/" title="" data-toggle="tooltip" data-placement="top" data-original-title="Compare">
                                                            <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Product;