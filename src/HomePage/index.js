import React from 'react';
import Slide from '../Slide';
import Product from '../Product';

class HomePage extends React.Component {
    render () {
        return (
            <div>
                <Slide/>
                <Product/>
            </div>
        )
    }
};

export default HomePage;